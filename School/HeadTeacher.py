from Person import Person

class HeadTeacher(Person):
    team_salary = 0
    salary = 0
    def __init__(
            self,
            name: str = None,
            surname: str = None,
            age: int = None,
            post_in_school: str = None,
            salary_rate: float = None,
                ):
        self.name = name
        self.surname = surname
        self.age = age
        self.post_in_school = post_in_school
        self.salary_rate = salary_rate
        HeadTeacher.team_salary += self.salary_rate
        print(f"Новий завуч на {self.surname} {self.name} створений ")

    def bonus(self):
        if self.salary == 0:
            print(f"Завуч {self.surname} {self.name} не отримує премію цього місяця")
        else:
            a = self.salary - self.salary_rate
            print(f"Завуч {self.surname} {self.name} цього місяця отримує премію у розмірі {a} грн")

        return self.salary








if __name__ == '__main__':
    A = HeadTeacher("te1ach","Wol",23, 'post', 1000)
    B = HeadTeacher("te1ach","Wol",23, 'post', 1000)
    print(A.team_salary,A.post_in_school)