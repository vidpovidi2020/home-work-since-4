from Person import Person

class Teacher(Person):
    team_salary = 0
    salary=0
    base_salary_rate = 6000
    def __init__(
        self,
        name: str = None,
        surname: str = None,
        age: int = None,
        post_in_school: str = None,
        salary_rate: float = None,
                ):
        self.name = name
        self.surname = surname
        self.age = age
        self.post_in_school = post_in_school
        self.salary_rate = salary_rate
        Teacher.team_salary += self.salary_rate
        print(f"Новий викладач на ім'я {self.name} створений ")



    def bonus(self):
        if self.salary == 0:
            print(f"Викладач {self.surname} {self.name} не отримує премію цього місяця")
        else:
            a = self.salary - self.salary_rate
            print(f"Викладач {self.surname} {self.name} цього місяця отримує премію у розмірі {a} грн")

        return self.salary



if __name__ == '__main__':
    A = Teacher("Teacher","Teacher1",40,"Teacher_post",6000)
    print(A.bonus())