#Hапишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:
#Попросіть користувача ввести свсвій вік.
#- якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
#- якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
#- якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
#- якщо вік користувача складається з однакових цифр (11, 22, 44 і тд років, всі можливі варіанти!) - вивести "О, вам <>! Який цікавий вік!"
#- у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів всеодно нема!"
#Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік
#Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача.
#Наприклад :
#"Тобі ж 5 років! Де твої батьки?"
#"Вам 81 рік? Покажіть пенсійне посвідчення!"
#"О, вам 33 роки! Який цікавий вік!"

#Зробіть все за допомогою функцій! Для кожної функції пропишіть докстрінг або тайпхінтінг.

#Не забувайте що кожна функція має виконувати тільки одну завдання і про правила написання коду.
#P.S. Для цієї і для всіх наступних домашок пишіть в функціях докстрінги або хінтінг



def get_positive_integer_from_user():
    '''
Функція  get_positive_integer_from_user отримує значення для віку користувача, аналізує отримані данні та зберігає. Аналізує чи є це значення позитивним числом та чи не перевищує максимальне значення
return: dict
    '''
    max_age = 120
    while True:
        user_input = input(f"Введіть свій вік цілим числом, яке не більше {max_age} ").lstrip('0')
        if user_input.isdigit() and int(user_input) <= max_age:
            result = {
                "user_age_int": int(user_input), 
                "user_age_str": user_input
            }
            return result
        print("Ви ввели некоректні дані")

def is_beautiful_age(visitor_age):
    '''
    Функція is_beautiful_age аналізує символи аргументу visitor_age та повертає булеві значення
    :param visitor_age: str
    :return: bool
    '''
    first_simbol = visitor_age[0]
    count_simbol = visitor_age.count(first_simbol)
    if len(visitor_age) > 1 and len(visitor_age) == count_simbol:
        return True
    else:
        return False


def year_declension(visitor_age):
    '''
    Функція  year_declension аналізує символи аргументу visitor_age та повертає слово "рік" у правильній формі
    :param visitor_age: str
    :return: bool
    '''

    if not visitor_age.isdigit():
        raise ValueError('Дані не можливо обробити')

    if len(visitor_age) > 1 and visitor_age[-2:] in ['12','13','14','15','16','17','18','19']:
        return 'років'

    last_sim = visitor_age[-1]

    if last_sim in '1':
        return 'рік'
    elif last_sim in '234':
        return 'роки'
    elif last_sim in '056789':
        return 'років'


def main():
    visitors_info = get_positive_integer_from_user()
    kras_vozr = is_beautiful_age(visitor_age=visitors_info["user_age_str"])
    final_year_declension = year_declension(visitor_age=visitors_info["user_age_str"])
    result_str = ''

    if kras_vozr:
        result_str = "О, вам {age} {declension}! Який цікавий вік!"
    elif visitors_info["user_age_int"] < 7:
        result_str = "Тобі ж {age} {declension}! Де твої батьки?"
    elif visitors_info["user_age_int"] < 16:
        result_str = "Тобі лише {age} {declension}, а це е фільм для дорослих!"
    elif visitors_info["user_age_int"] > 65:
        result_str = "Вам {age} {declension}? Покажіть пенсійне посвідчення!"
    else:
        result_str = "Незважаючи на те, що вам {age} {declension}, білетів всеодно нема!"


    return result_str.format(age=visitors_info["user_age_int"], declension=final_year_declension)


if __name__ == '__main__':
    print(main())






