#Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів і цінами: { "cilpio": 47.999, "a_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.ua": 38.324, "my-store": 37.166, "the_partner": 38.988, "sto": 37.720, "rozetka": 38.003}. Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною. Наприклад:
lower_limit = 38.0
upper_limit = 39.0
#> match: "my-store", "main-service"
list_value = []
store_price = { "cilpio": 47.999, 
"a_studio": 42.999, 
"momo": 49.999, 
"main-service": 37.245, 
"buy.ua": 38.324, 
"my-store": 37.166, 
"the_partner": 38.988, 
"sto": 37.720, 
"rozetka": 38.003}

for shop, price in store_price.items():
     if price >= lower_limit and price <= upper_limit:
          print(shop,price)








