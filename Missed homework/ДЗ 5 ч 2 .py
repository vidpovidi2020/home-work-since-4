
#Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float. Якщо перетворити не вдається функція має повернути 0 (флоатовий нуль).

def type_change_function (arg):

    """
    Function for conversion to type float any input data. If the data cannot be converted gives 0.00

     :param arg: data from input
     :type arg: any
     :return:  None
#     """

    
   
    while True:
        try:
            print(f'Ви ввели {arg}, тому результат float:',float(arg))
        except:
            print('Введені данні не можливо перетворити у тип float, тому результат:',float(0))
        break
data = input("введіть данні для перетворення у тип float: ")
type_change_result = type_change_function(data)
