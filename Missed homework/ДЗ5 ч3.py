#Напишіть функцію, що приймає два аргументи. Функція повинна
#якщо аргументи відносяться до числових типів - повернути різницю цих аргументів,
#якщо обидва аргументи це строки - обʼєднати в одну строку та повернути
#якщо перший строка, а другий ні - повернути dict де ключ це перший аргумент, а значення - другий
#у будь-якому іншому випадку повернути кортеж з цих аргументів



def analyse_data_and_action(arg1, arg2):
    """
    Функциія виконує дії над аргументами в залежності від їх типів данних.
    Різницю аргументів, якщо arg1 та arg2 одного типа (int або float); 
    суму аргументів, якщо arg1 та arg2 одного типа (str); 
    словник, якщо тільки перший аргумент arg1 = str; кортеж в інших випадках. 
    :param arg1:int,float,str
    :param arg2:int,float,str
    :return: int,str, dict, tuple
    """
    while type(arg1) == type(arg2):
        if type(arg1) == int or type(arg1) == float:
            return arg1 - arg2
        elif type(arg1) == str:
            return arg1 + arg2
    if type(arg1) == str:   
        my_dict = {arg1:arg2}
        return my_dict
    elif type(arg1) != str:
        my_tuple = (arg1,arg2)
        return my_tuple
d = analyse_data_and_action(22, "м")
print ("Результатом обчислень є данні типу",type(d),"та сам результат дорівнює",d)