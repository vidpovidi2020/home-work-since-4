"""
select_type             - функція пропонує користувачу обрати тип відпустки, отримує данні користувача та викликає обрану декоровану функцію.
                        На введені користувачем дані немає ніякої валідації, дату можна вказувати числами та текстом.
                        На етапі вибору виду нерабочого періоду користувач повинен ввести одне з наступних значень:
                        відпустка, vacation, отпуск, лікарняний, sick, больничный, вихідний, day off,выходной, допускаються пробіли до і після слова.

decorator_function      - функція, яка декорує типи неробочих періодів


Vacation_pattern        - функції відповідних типів неробочих періодів, складаються лише з однієї строки, бо відрізняються між собою люше однією строкої, тому
Sick_Leave_Pattern      однакові строки були додані до функції декоратора. Приймаю всі данні, які були введені користувачем.
Day_Off_Patternn

"""
def select_type():
    type_of_vacation = input('Введіть вид запросу: відпустка або лікарняний, або вихідний:')
    type_of_vacation = type_of_vacation.strip()
    print(f"Ви обрали {type_of_vacation}")
    a = ('відпустка', 'vacation','отпуск')
    b = ('лікарняний','sick', 'больничный')
    c = ('вихідний','day off','выходной')
    if type_of_vacation not in a and type_of_vacation not in b and type_of_vacation not in c:
        print('Ви ввели невірні данні')
        return select_type()
    First_Name = input('Введіть своє і\'мя:')
    Surname = input('Введіть своє прізвище:')
    from_date = input('Введіть дату початку:')
    to_date = input('Введіть дату закінчення:')
    if type_of_vacation in a:
        return Vacation_pattern(First_Name,Surname,from_date,to_date)
    elif type_of_vacation in b:
        return Sick_Leave_Pattern(First_Name,Surname,from_date,to_date)
    elif type_of_vacation in c:
        return Day_Off_Patternn(First_Name,Surname,from_date,to_date)




def decorator_function(func):
    def wrapper(First_Name,Surname,from_date,to_date):
        print('=========================================>')
        print(f'Виконується функція:{func.__name__}')
        print('')
        print('CEO Red Bull Inc.')
        print('Mr. John Bigbull')
        print('Hi John,')
        func(First_Name,Surname,from_date,to_date)
        print(f'{First_Name} {Surname}')
        return func
    return wrapper

@decorator_function
def Vacation_pattern(First_Name,Surname,from_date,to_date):
    print(f'I need the paid vacations from {from_date} to {to_date}')


@decorator_function
def Sick_Leave_Pattern(First_Name,Surname,from_date,to_date):
    print(f'I need the paid sick leave {from_date} to {to_date}')

@decorator_function
def Day_Off_Patternn(First_Name,Surname,from_date,to_date):
    print(f'I need the paid day off {from_date} to {to_date}')

if __name__ == "__main__":
    select_type()
