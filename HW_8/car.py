class Vehicle:
    def __init__(
        self,
        producer: str = None,
        model: str = None,
        year: int = None,
        tank_capacity: float = None,
        tank_level: float = 0.0,
        maxspeed: int = None,
        fuel_consumption : float = None, 
        odometer_value: int = 0
        ):


        self.producer = producer
        self.model = model
        self.year = year
        self.tank_capacity = tank_capacity
        self.tank_level = tank_level
        self.maxspeed = maxspeed
        self.fuel_consumption = fuel_consumption
        self.odometer_value = odometer_value 

    def __repr__(self) -> str:
        return f"Об'єкт классу Vehicle, а саме модель {self.model},  {self.year} року випуску, яка має {self.odometer_value} кілометрів пробугу"

    def refueling(self):
        delta = self.tank_capacity-self.tank_level
        self.tank_level = self.tank_capacity
        print(f'{delta}  літрів було заправлено')

    def race(self, distance_for_future_travel:float):
        available_travel_distance = (self.tank_level/self.fuel_consumption)*100
        print(available_travel_distance)

        if available_travel_distance < distance_for_future_travel:
            distance_for_future_travel = available_travel_distance


        self.odometer_value += distance_for_future_travel
        travel_time = distance_for_future_travel/(self.maxspeed*0.8)
        self.tank_level -= (distance_for_future_travel/100)*self.fuel_consumption
        exit = {
            "self.odometer_value": self.odometer_value,
            "self.tank_level": self.tank_level,
            "travel_time": travel_time 
        }
        return exit

        

    def lend_fuel(self,other):
        required_amount_of_petrol = self.tank_capacity - self.tank_level
        if required_amount_of_petrol == 0.0 or other.tank_level == 0:
            print("Нічого страшного, якось розберуся")
            
        elif required_amount_of_petrol <= other.tank_level:
            self.tank_level += required_amount_of_petrol
            other.tank_level -= required_amount_of_petrol
            print(f"Дякую, бро, виручив. Ти залив мені {required_amount_of_petrol} літрів пального") 

        elif required_amount_of_petrol > other.tank_level:
            required_amount_of_petrol = other.tank_level
            print(f"Дякую, бро, виручив. Ти залив мені {required_amount_of_petrol} літрів пального")
            
        


    def get_tank_level(self):
        return self.tank_level


    def get_mileage(self):
        return self.odometer_value

    def __eq__(self, other):
        return self.year == other.year and self.odometer_value == other.odometer_value

     
        

        

  




if __name__ == '__main__':
    

    bmw = Vehicle(
        producer='BMW',
        model='X5', 
        year=2020,
        tank_capacity=70, 
        tank_level=20.0,
        maxspeed=90,
        fuel_consumption=15.0)

    audi = Vehicle(
        producer = 'Audi',
        model = 'Q7', 
        year= 2020,
        tank_capacity = 80,
        tank_level = 10.0,
        maxspeed = 90, 
        fuel_consumption = 15.0)
    print (bmw.__repr__())
    print (audi.__repr__())
    print (bmw.get_tank_level())
    print (audi.get_tank_level())
    print (bmw.refueling())
    print (audi.refueling())    
    print (bmw.race(220))
    print (audi.race(470))      
    print (audi.lend_fuel(bmw)) 
    print (audi.get_tank_level())
    print (bmw.get_mileage())
    print (audi.get_mileage())  
    print (audi.__eq__(bmw))
   





    
 
 