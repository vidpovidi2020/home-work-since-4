
#Завдання №1
#Розробити клас Людина. Людина має: Ім'я Прізвище  Вік (атрибут але ж змінний) Стать
#Люди можуть: Їсти Спати Говорити Ходити Стояти  Лежати
#Також ми хочемо контролювати популяцію людства. Змінювати популяцію можемо в __init__. 
#Треба сказати, що доступ до статичних полів класу з __init__ не можу іти через НазваКласу.статичий_атрибут, позаяк ми не може бачити імені класу. 
# Але натомість ми можемо сказати self.__class__.static_attribute.

import datetime

class Person:
    population = 0
    def __init__(
        self,
        name:str = None,
        surname:str = None,
        birth_day:str = None,
        sex:str = None):
        self.name = name
        self.surname = surname
        self.sex = sex
        self.birth_day = birth_day
        self.count = Person.population+1
        Person.population +=1

    @property
    def population_with_person(self):
        return self.count
    
    @property
    def age(self):
        date_split_for_age = self.birth_day.split(',')
        h = [int(i) for i in date_split_for_age]
        age = (datetime.date.today() - datetime.date(h[0],h[1],h[2])).days // 365
        return age

    def Eating_food(self, food):
        print (f'Person eating {food}')

    def Sleep (self, hours_of_sleep):
        print (f'Person sleep {hours_of_sleep} hours')
    
    def Speak(self):
        print ('Person communicate with each other')

    def Walk(self,distance):
        print (f'Person walk about{distance}')

    def Stand(self):
        print ('Person stand')
    
    def Lie(self):
        print ('Person Lie')
    




    

 
if __name__ == '__main__':
    Old = Person(
        name = "Tom",
        surname="Tomer",
        sex = "man",
        birth_day = '1900,12,30'
        )

    Young = Person(
        name = "Mary",
        birth_day = '2001,09,01'
    )
      
    child = Person(
        name = "Ivy",
        birth_day = '2020,02,20'
    )


    print("Загальна кількість об'єктів класу \"Person\":  ---> ",Person.population)
    print('Вік кожної особи в попудяції: ---->',Old.age, Young.age, child.age)
    print(Old.Eating_food("vegetables"),Young.Lie(), child.Sleep(10))

