#Візьміть свою HW 6 ("Касир в кінотеатрі"), винесіть всі допоміжні функціі в окремий файл. В основному файлі виконайте імпорт цих функцій.
def get_positive_integer_from_user():
    '''
Функція  get_positive_integer_from_user отримує значення для віку користувача, аналізує отримані данні та зберігає. Аналізує чи є це значення позитивним числом та чи не перевищує максимальне значення
return: dict
    '''
    max_age = 120
    while True:
        user_input = input(f"Введіть свій вік цілим числом, яке не більше {max_age} ").lstrip('0')
        if user_input.isdigit() and int(user_input) <= max_age:
            result = {
                "user_age_int": int(user_input), 
                "user_age_str": user_input
            }
            return result
        print("Ви ввели некоректні дані")

def is_beautiful_age(visitor_age):
    '''
    Функція is_beautiful_age аналізує символи аргументу visitor_age та повертає булеві значення
    :param visitor_age: str
    :return: bool
    '''
    first_simbol = visitor_age[0]
    count_simbol = visitor_age.count(first_simbol)
    if len(visitor_age) > 1 and len(visitor_age) == count_simbol:
        return True
    else:
        return False


def year_declension(visitor_age):
    '''
    Функція  year_declension аналізує символи аргументу visitor_age та повертає слово "рік" у правильній формі
    :param visitor_age: str
    :return: bool
    '''

    if not visitor_age.isdigit():
        raise ValueError('Дані не можливо обробити')

    if len(visitor_age) > 1 and visitor_age[-2:] in ['11','12','13','14','15','16','17','18','19']:
        return 'років'

    last_sim = visitor_age[-1]

    if last_sim in '1':
        return 'рік'
    elif last_sim in '234':
        return 'роки'
    elif last_sim in '056789':
        return 'років'




