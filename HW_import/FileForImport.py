import FunctionLibrary
""""
Функція Receiving_and_processing_data використовує функції з файлу FunctionLibrary та повертає результат.
"""
def Receiving_and_processing_data():
    visitors_info = FunctionLibrary.get_positive_integer_from_user()
    kras_vozr = FunctionLibrary.is_beautiful_age(visitor_age=visitors_info["user_age_str"])
    final_year_declension = FunctionLibrary.year_declension(visitor_age=visitors_info["user_age_str"])
    result_str = ''

    if kras_vozr:
        result_str = "О, вам {age} {declension}! Який цікавий вік!"
    elif visitors_info["user_age_int"] < 7:
        result_str = "Тобі ж {age} {declension}! Де твої батьки?"
    elif visitors_info["user_age_int"] < 16:
        result_str = "Тобі лише {age} {declension}, а це е фільм для дорослих!"
    elif visitors_info["user_age_int"] > 65:
        result_str = "Вам {age} {declension}? Покажіть пенсійне посвідчення!"
    else:
        result_str = "Незважаючи на те, що вам {age} {declension}, білетів всеодно нема!"


    return result_str.format(age=visitors_info["user_age_int"], declension=final_year_declension)









if __name__ == '__main__':
    print(Receiving_and_processing_data())
