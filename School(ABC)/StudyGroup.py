from Schoolchild import Schoolchild
from Teacher import Teacher


class StudyGroup:
    max = 10
    count_of_study_groups = 0
    def __init__(self,title=str,class_teacher= Teacher, schoolchild_1 =Schoolchild,schoolchild_2 =Schoolchild,schoolchild_3 = Schoolchild,schoolchild_4 = Schoolchild,schoolchild_5 = Schoolchild,):
        self.title = title
        self.class_teacher = class_teacher
        self.schoolchild_1 = schoolchild_1
        self.schoolchild_2 = schoolchild_2
        self.schoolchild_3 = schoolchild_3
        self.schoolchild_4 = schoolchild_4
        self.schoolchild_5 = schoolchild_5
        print(f"Сформовано новий учбовий клас під назвою {self.title}")


    def __new__(cls, *args, **kwargs):
        if cls.count_of_study_groups >= cls.max:
            print("Новий учбовий клас не можливо створити. Всі можливі учбові класи вже створені")
        else:
            cls.count_of_study_groups = cls.count_of_study_groups + 1
            a = cls.max - cls.count_of_study_groups
            print(f"Новий учбовий клас з номером {cls.count_of_study_groups} створено. В данній школі можна створити ще {a} класів")
        return super(StudyGroup, cls).__new__(cls)


    @property
    def average_score_recalculation(self):
        average_score = (self.schoolchild_1.average_grade + self.schoolchild_2.average_grade+self.schoolchild_3.average_grade+self.schoolchild_4.average_grade+self.schoolchild_5.average_grade)/5
        high_score=10
        if average_score >= high_score:
            self.class_teacher.salary = self.class_teacher.salary_rate*1.20
            print(f"Керівник класу {self.title} отримує премію цього місяця, бо середній бал класу складає {average_score}. Зарплата з премією становить {self.class_teacher.salary} ")
        else:
            print(f"Класний керівник не отримує премію цього місяця, бо середній бал класу складає {average_score}")
        return average_score




if __name__ == '__main__':

    Pupil_1 = Schoolchild("Scoolchild", "Wol1", 23, 11)
    Pupil_2 = Schoolchild("Scoolchild", "Wol2", 23, 12)
    Pupil_3 = Schoolchild("Scoolchild", "Wol3", 23, 10)
    Pupil_4 = Schoolchild("Scoolchild", "Wol4", 23, 10)
    Pupil_5 = Schoolchild("Scoolchild", "Wol5", 23, 11)
    Teacher_1 = Teacher("Teacher","Teacher1",40,"Teacher_post",6000)
    print(O.average_score_recalculation())


