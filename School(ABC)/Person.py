from abc import ABC
def abstractmethod(args):
    pass


class Person(ABC):
    def __init__(
        self,
        name: str = None,
        surname :str = None,
        age:int = None,
        post_in_school:str=None,
        salary_rate:float=None,
        ):
        self.name = name
        self.surname = surname
        self.age = age
        self.post_in_school = post_in_school
        self.salary_rate = salary_rate

    @abstractmethod
    def bonus(self):
        pass

    def professional_activity(self):
        print(f"Робітник {self.surname} {self.name}, який обіймає посаду {self.post_in_school} та сумлінно виконує свої службові обов'язки")





if __name__ == '__main__':
    Person1 = Person()