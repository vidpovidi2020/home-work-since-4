"""
___________________________________________________________________________________________Завдання___________________________________________________________________________________________________________
Підґрунтя
Приватна школа надає непогані послуги по освіті. Прекрасн аудиторії, чисті туалети... Вмотивовані вчителі... Та за все треба платити...
Технічне завдання
Розробити програму, яка підрахує на основі зарплат вчителів, директора, заучів та прибиральників, скільки дітей та з якою оплатою за навчання треба формувати класи на наступний навчальний рік.
Рівно так само програма має виводити середню оцінку учнів по класу і загалом по школі (для спрощення завдання учневі ми приписуємо лише середній бал).
Деталі

Кожна людина має:
Імʼя
Прізвище
вік
посада згідно штатного розпису
Персонал школи є людьми, які мають:
Зарплату
В школі є наступний персонал:
Директор (зп - 20000 грн в місяць)
Завуч (зп - 15000 грн в місяць)
Вчитель (зп - 6000 грн в місяць)
Учень теж належить до персоналу, але: він платить за навчання... І якщо персонал отримує винагороду через отримання зарплати, то учні отримують винагороду через гарні оцінки!
(врахуйте це. Отимання винагороди - абстрактна дія, яка для певних видів персоналу означає різне).
Школа містить у собі класів. Кожен клас має список учнів. Учнів можна до класу додавати, видаляти, виводити список учнів на консоль. Також клас має учителя - класного керівника, що веде клас.
Класи можна додавати (додається пустий клас, але з класним керівником), видаляти (інші учні мають бути або переведеним в інший клас, або виключеним зі школи).
______________________________________________________________________________________________________________________________________________________________________________________________________________




    Внесу деякі доповнення, для більшою реалістичності та зрозумілості програми.
    
    Приватна школа - приватне підприємство, яке надає послуги в сфері освіти. Школа обов'ясково має приміщення (орендоване чи власне), обмежену кількість аудиторій та місць для учнів.
    Також приватна школа повинна слідкувати за ринковую вартістю послуг, яка скадалає 6000 грн на місяць. Школа має 10 приміщень, кожне з яких обладнане для навчання 5 учнів.
    Школа має лише одного директора, 2х завучів, 10 вдителів загальних предметів та класних керівників.

    
    Опишу свої розрахунки словами:
    School.price_for_pupil - обчислює вартість навчання для існуючої кількості учнів, якщо вартість вища за ринкову обчислює кількість учнів для необхідну для "виходу" на ринковий рівень.
                             Також обчислює найменшу вартість навчання для учнів, якщо набрати маскимальну кількість,відповідно можливості школи та з урахуванням всіх можливих витрат (оренда, премії)

    School.school_grade_point_average - обчислює середній бал по школі, якщо він більше 10 директор, завучі та прибиральник отримують премію.
                                        Вчителі отримують премії якщо середній бал його класу більший за 10(StudyGroup.average_score_recalculation)

    StudyGroup.average_score_recalculation - обчислює середній бал класу, залежно від результату встановлює премію для вчителя класу

    StudyGroup.count_of_study_groups - кільксть творених класів в школі

    .bonus - абстрактний метод, який обчислює суму премії для робітників школи та винагороду для учнів
    
    
"""
from StudyGroup import StudyGroup
from Director import Director
from HeadTeacher import HeadTeacher
from Schoolchild import Schoolchild
from Cleaner import Cleaner
from Teacher import Teacher

class School:
    students_place_in_classroom = 5
    number_of_auditors = 10
    rent = 50000
    market_value = 6000

    def __init__(
            self,
            title=str,
            director_of_school=Director,
            head_teacher_1=HeadTeacher,
            head_teacher_2=HeadTeacher,
            cleaner_of_school=Cleaner,
            study_groups_of_school=list(),
    ):
        self.title = title
        self.director_of_school = Director
        self.head_teacher_1 = head_teacher_1
        self.head_teacher_2 = head_teacher_2
        self.cleaner_of_school = cleaner_of_school
        self.study_groups_of_school = study_groups_of_school
        print(f"Створено школу під назвою {self.title}")

    @property
    def price_for_pupil(self):
        total_school_cost = (self.rent + self.director_of_school.salary_rate + HeadTeacher.team_salary + Cleaner.team_salary + Teacher.team_salary) / Schoolchild.pupils
        max_total_school_cost = self.rent + (self.director_of_school.salary_rate + self.head_teacher_1.salary_rate * 2+self.cleaner_of_school.salary_rate + Teacher.base_salary_rate * 10)*1.2
        max_schoolchild_pupils = self.number_of_auditors * self.students_place_in_classroom
        min_total_school_cost = max_total_school_cost / max_schoolchild_pupils
        schoolchild_pupils_for_market_value = int(max_total_school_cost/ self.market_value)
        if total_school_cost > School.market_value:
            print(f"Вартість навчання в школі {self.title} складає {total_school_cost} це вище за ринкову вартість")
            print(f"Мінімальная вартість навчання в школі {self.title} складає {min_total_school_cost}, якщо набрати макскимальну кількість учнів, яка складає {max_schoolchild_pupils} учнів")
            print(f"Мінімальная кількість учнів школи {self.title} складає {schoolchild_pupils_for_market_value} учнів, при якій вартість навчання не привищує ринкову")
        else:
            print(f"Вартість навчання складає {total_school_cost} це відповідає ринковій вартості")
        return "Поточна вартість навчання:", total_school_cost

    @property
    def school_grade_point_average(self):
        high_score = 10
        school_grade_point_average = Schoolchild.total_score / Schoolchild.pupils
        if school_grade_point_average >= high_score:
            self.director_of_school.salary = self.director_of_school.salary_rate*1.2
            self.head_teacher_1.salary = self.head_teacher_1.salary_rate * 1.2
            self.head_teacher_2.salary = self.head_teacher_2.salary_rate * 1.2
            self.cleaner_of_school.salary = self.cleaner_of_school.salary_rate * 1.2
        else:
            print(f"Середній бал по школі {school_grade_point_average}")
        return " Середній бал по школі:",school_grade_point_average


if __name__ == '__main__':
    Director = Director("Yaroslav", "Mudryi", 54, "Director of school", 20000)
    HeadTeacher1 = HeadTeacher("Bohdan", "Khmelnytskyi", 45, "First head teacher of school", 15000)
    HeadTeacher2 = HeadTeacher("Valerii", "Lobanovskyi", 47, "Second head teacher of school", 15000)
    Teacher1 = Teacher("Ivan", "Franko", 42, "Teacher1 of school", 6000)
    Teacher2 = Teacher("Ivan", "Mazepa", 28, "Teacher2 of school", 6000)
    Teacher3 = Teacher("Volodymyr", "Velykyi", 55, "Teacher3 of school", 6000)
    Teacher4 = Teacher("Mykola", "Hohol", 47, "Teacher4 of school", 6000)
    Teacher5 = Teacher("Serhii", "Korolov", 33, "Teacher5 of school", 6000)
    Cleaner_of_school = Cleaner("Vasyl", "Stus", 40, "Cleaner of school", 5000)
    Pupil_1 = Schoolchild("Pupil_1", "Pupil_1", 23, 10)
    Pupil_2 = Schoolchild("Pupil_2", "Pupil_2", 23, 10)
    Pupil_3 = Schoolchild("Pupil_3", "Pupil_3", 23, 12)
    Pupil_4 = Schoolchild("Pupil_4", "Pupil_4", 23, 10)
    Pupil_5 = Schoolchild("Pupil_5", "Pupil_5", 23, 11)
    Pupil_6 = Schoolchild("Pupil_6", "Pupil_6", 23, 7)
    Pupil_7 = Schoolchild("Pupil_7", "Pupil_7", 23, 11)
    Pupil_8 = Schoolchild("Pupil_8", "Pupil_8", 23, 9)
    Pupil_9 = Schoolchild("Pupil_9", "Pupil_9", 23, 10)
    Pupil_10 = Schoolchild("Pupil_10", "Pupil_10", 23, 11)
    StudyGroup1 = StudyGroup("Початковий", Teacher1, Pupil_1, Pupil_2, Pupil_3, Pupil_4, Pupil_5)
    StudyGroup2 = StudyGroup("Середній", Teacher2, Pupil_6, Pupil_7, Pupil_8, Pupil_9, Pupil_10)
    OurSchool = School("OurSchool ", Director, HeadTeacher1, HeadTeacher2, Cleaner_of_school,[StudyGroup1, StudyGroup2])
    print(OurSchool.school_grade_point_average)
    print(OurSchool.price_for_pupil)
    print(OurSchool.head_teacher_1.salary)
    print(StudyGroup1.count_of_study_groups)
    print(StudyGroup1.average_score_recalculation, StudyGroup2.average_score_recalculation)

