from Person import Person

class Cleaner(Person):
    salary = 0
    team_salary = 0
    def __init__(
        self,
        name: str = None,
        surname: str = None,
        age: int = None,
        post_in_school: str = None,
        salary_rate: float = None,
                ):
        self.name = name
        self.surname = surname
        self.age = age
        self.post_in_school = post_in_school
        self.salary_rate = salary_rate
        Cleaner.team_salary += self.salary_rate
        print(f"Новий прибиральник  {self.surname} {self.name} створений ")

    def bonus(self):
        if self.salary == 0:
            print(f"Прибиральник {self.surname} {self.name} не отримує премію цього місяця")
        else:
            a = self.salary - self.salary_rate
            print(f"Прибиральник {self.surname} {self.name} цього місяця отримує премію у розмірі {a} грн")

        return self.salary

if __name__ == '__main__':
    A = Cleaner("New","Wol",50, "Cleaner", 5000)
    print(A.professional_activity())